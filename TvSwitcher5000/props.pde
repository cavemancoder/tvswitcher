/**
 * simple convenience wrapper object for the standard
 * Properties class to return pre-typed numerals
 */
class P5Properties extends Properties {

  boolean getBooleanProperty(String id, boolean defState) {
    return boolean(getProperty(id, ""+defState));
  }

  int getIntProperty(String id, int defVal) {
    return int(getProperty(id, ""+defVal));
  }

  float getFloatProperty(String id, float defVal) {
    return float(getProperty(id, ""+defVal));
  }
}

public class props {
  P5Properties propfile;

  P5Properties screenProfile;
  String serverIP;
  String serverPort;
  InetAddress consoleIP;
  int roleID;
  int showID;
  int curProfileNum= 0;
  boolean useCustomList;
  String[][] screen = new String[NUMOFSCREENS][4];
  String[] screenName = new String[NUMOFSCREENS];
  String[] profileFiles = new String[6];
  String[] profileNames = new String[6];
  ArrayList<macro> macros = new ArrayList<macro>();
  ArrayList<String> channels;
  props() {
    try {
      propfile=new P5Properties();
      // load a configuration from a file inside the data folder
      propfile.load(openStream("properties.txt"));
      //Server and Console Setup
      this.serverIP=propfile.getProperty("setup.serverIP", "192.168.1.121");
      this.serverPort = propfile.getProperty("setup.serverPort", "2638");
      this.consoleIP=InetAddress.getByName(propfile.getProperty("setup.consoleIP", "192.168.1.102"));
      this.roleID=propfile.getIntProperty("setup.roleID", 8);
      this.showID=propfile.getIntProperty("setup.showID", 3);
      //Load Channel Definition
      channels = new ArrayList<String>(Arrays.asList(split(propfile.getProperty("channels", "0"), ",")));
      for (int i=0;i<channels.size();i++)
      {
        for (int j=0;j<NUMOFTVS;j++) {
          TVddl[j].addItem(channels.get(i), int(split(channels.get(i), "-")[0]));
        }
      }
      //Get Profile Names
      profileFiles[0] = "properties.txt";
      profileNames[0] = "Default";
      Profileddl.addItem("Default",0);
      for (int i=1;i<6;i++)
      {
        profileFiles[i] = propfile.getProperty("profile" + i, null);
        screenProfile = new P5Properties();
        if (openStream(profileFiles[i]) !=null) {
          try {    
            screenProfile.load(openStream(profileFiles[i]));
            profileNames[i] = screenProfile.getProperty("name","");
            Profileddl.addItem(profileNames[i], i);

          }
          catch(IOException e) {
          }
          
        }
      }
      //Load Default Screens
      loadScreenProfile(0);
    } 
    catch(IOException e) {
      println("couldn't read config file...");
    }
  }


  void loadScreenProfile(int num)
  {
    try {
      curProfileNum = num;
      screenProfile = new P5Properties();
      screenProfile.load(openStream(profileFiles[num]));
      macros.clear();
      Profilefield.setText(profileNames[num]);
      useCustomList = screenProfile.getBooleanProperty("usecustom", false);
      if (useCustomList) {
        curState.refresh(props.roleID);
      }
      for (int i=0;i<NUMOFSCREENS;i++)
      {
        screenLbl[i].setText(" ");
        this.screenName[i] = screenProfile.getProperty("screen" + (i+1) +"." + "name", " ");
        if (this.screenName[i].length() > 8)
          screenLbl[i].setText(this.screenName[i].substring(0, 7));
        else
          screenLbl[i].setText(this.screenName[i]);
        for (int j = 0;j<4;j++)
        {
          this.screen[i][j] = screenProfile.getProperty("screen" + (i+1) +"." + (j+1), "0");
        }
      }
      for ( int i=1;i<=NUMOFSCREENS;i++) {
        if (screenProfile.containsKey("macro"+i+".delay")&&screenProfile.containsKey("macro"+i+".channels")) {
          macros.add(new macro(screenProfile.getIntProperty("macro"+i+".delay", 0), screenProfile.getProperty("macro"+i+".channels", "0")));
        }
      }
    }
    catch(IOException e) {
      println("couldn't read config file...");
    }
  }
}

